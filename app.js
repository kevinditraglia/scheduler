var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var redis = require('redis').createClient();

var storeMessage = function(name, data) {
  var message = JSON.stringify({name: name, data: data});
  
  redis.lpush("messages", message);
};

io.on('connection', function(client){
  client.on('disconnect', function(name){
    console.log(client.nickname + " disconnected");
    client.broadcast.emit("remove chatter", client.nickname);

    var message = "has left the chat.";
    io.emit('chat message', client.nickname + ": " + message);
    storeMessage(client.nickname, message);
    redis.srem("chatters", client.nickname);
  });

  client.on('join', function(name) {
    client.nickname = name;
    client.broadcast.emit("add chatter", name);
    redis.sadd("chatters", name);

    var message = "has joined the chat.";
    io.emit('chat message', client.nickname + ": " + message);
    storeMessage(client.nickname, message);

    redis.smembers('chatters', function(err, names) {
     names.forEach(function(name){
      client.emit('add chatter', name);
     });
    });

    redis.lrange("messages", 0, -1, function(err, messages){
      messages = messages.reverse();
      messages.forEach(function(message) {
        message = JSON.parse(message);
        client.emit("chat message", message.name + ": " + message.data);
      });
    });
    console.log(client.nickname + " connected");
  });

  client.on('chat message', function(msg){
    var name = client.nickname;
    io.emit('chat message', name + ": " + msg);
    storeMessage(name, msg);
  });
});

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/client.js', function(req, res){
  res.sendFile(__dirname + '/client.js');
});

// Listen on port 8000, IP defaults to 127.0.0.1
http.listen(8000);

// Put a friendly message on the terminal
console.log("Server running at http://127.0.0.1:8000/");
