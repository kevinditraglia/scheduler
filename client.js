var server = io();
server.on('connect', function(data) {
  nickname = prompt("What is your name?");

  server.emit('join', nickname);
});
$('form').submit(function(){
  server.emit('chat message', $('#m').val());
  $('#m').val('');
  return false;
});
server.on('chat message', function(msg){
  $('#messages').append($('<li>').text(msg));
});
server.on('add chatter', function(name) {
var chatter = $('<li>'+name+'</li>').data('name', name);
 $('#chatters').append(chatter);
});
server.on('remove chatter', function(name) {
 $('#chatters li[name='+ name + ']').remove();
});